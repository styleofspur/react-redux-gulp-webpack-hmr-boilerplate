import gulp      from 'gulp';
import pluginsFn from 'gulp-load-plugins';
import fs        from 'graceful-fs';
import path      from 'path';
import yargs     from 'yargs';

import paths   from './build/paths.js';
import aliases from './build/gulp/aliases.js';

const plugins = pluginsFn();
let options = yargs
    .alias('e', 'env')
    .alias('w', 'watch')
    .default({env: 'dev'})
    .argv;

try {

    // Load aliases
    aliases(gulp, plugins, options);

    // Load all tasks
    fs
        .readdirSync(paths.gulp.tasks)
        .forEach((filename) => {
            let file = path.join(paths.gulp.tasks, filename);
            let stat = fs.statSync(file);

            if (stat.isFile() && filename.slice(-3) !== '.js') return;

            let name = filename.slice(0, -3);
            let task = require(paths.gulp.tasks +'/'+ filename)(gulp, plugins, options);

            // Register the task with `prv-` prefix as private one
            gulp.task(`prv-${name}`, task);
        });
} catch(e) {
    console.error(e);
}
