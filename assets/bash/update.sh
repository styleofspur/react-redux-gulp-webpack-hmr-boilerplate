#!/bin/bash

env="$1"

cd /my/websites/my-project/
sudo -u www-node git pull
npm install
sudo -u www-node gulp build -e $env
pm2 restart my-project