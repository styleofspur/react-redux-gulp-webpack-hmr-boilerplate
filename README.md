# My Project

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Install](#markdown-header-install)
    - [Global packages](#markdown-header-global-packages)
    - [Local packages](#markdown-header-local-packages)
- [Usage](#markdown-header-usage)
    - [Build All](#markdown-header-build-all)
    - [Watch Client](#markdown-header-watch-client)
    - [Watch Server](#markdown-header-watch-server)
- [You're ready to go.](#markdown-header-youre-ready-to-go)
- [Hot Module Replacement](#markdown-header-hot-module-replacement)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Install
### Global packages

- Node 6
- Babel 6
- Gulp 3.9.1

### Local packages
`npm install`

## Usage

### Build All
Run `gulp build`

### Watch Client
Run `gulp watchClient`

### Watch Server
Run `gulp watchServer`

## You're ready to go.
Run `npm start`, you should get this output:

`HTTP Server started at port: XXXX` 

## Hot Module Replacement

[HMR](https://webpack.github.io/docs/hot-module-replacement.html) is enabled by default only in **dev** environment.
It's implemented on **server** with :
- [webpack-dev-middleware](https://github.com/webpack/webpack-dev-middleware) 
- [webpack-hot-middleware](https://github.com/glenjamin/webpack-hot-middleware) 

Both in `./src/server/middlewares/webpack.js` file.
 
On **client** in:
- `./src/client/bootstrap.js` for router views reload support
- `./src/client/store.js` for redux store reload support
- `./src/client/pages/home` for default router view reload support 
- `./src/client/utils/enums/hub` for enums reload support 

As you can see in these files you need to add the next code:

1. for making current file to be reloadable - add the next code in the end of each hub/singular file: 
```javascript
if (module.hot) {
    module.hot.accept(); 
}
```

2. for making any directory to be reloadable - add the next code in the file you want to set it up: 
```javascript
if (module.hot) {
    module.hot.accept('./directory' /* or [dir1, dir2, ... ] */); 
}
```

3. in order to add any callback for execute some code on reload: 
```javascript
if (module.hot) {
    module.hot.accept(string | [string] , () => {
        /* your code here */
    });
}
```