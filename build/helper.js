import del from 'del';

let buildHelper = {
    get babelServerOptions(){
        return {
            plugins: [
                "add-module-exports"
            ],
            presets: [
                "node6",
                "stage-0"
            ]
        };
    },

    get babelClientOptions(){
        return {
            plugins: [
                "add-module-exports",
                "transform-class-properties"
            ],
            presets: [
                "stage-0",
                "es2015",
                "react"
            ]
        };
    },
    async removeDir(path) {
        return del(path, {force: true});
    }
};

export default buildHelper;