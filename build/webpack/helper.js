export default class WebpackHelper{

    _logger;

    constructor(logger){
        this._logger = logger;
    }

    handleOutput(stats) {
        this._logger.log('[webpack:build]', stats.toString({
            colors:       true,
            warnings:     true,
            errors:       true,
            chunks:       false,
            version:      false,
            assets:       false,
            timings:      false,
            entrypoints:  false,
            chunkModules: false,
            chunkOrigins: false,
            cached:       false,
            cachedAssets: false,
            reasons:      false,
            usedExports:  false,
            children:     false,
            source:       false,
            modules:      false
        }));
    }
};
