import webpack           from 'webpack';
import path              from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import buildHelper from '../helper';

export default (options = {}) => {
    const isProd = options.env === 'prod';

    const paths   = options.paths || require('../paths');
    const context = path.resolve(paths.client.src);
    const resolve = {
        modulesDirectories: ['node_modules'],
    };

    const loaders = [
        // ES6
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel',
            query: buildHelper.babelClientOptions
        },
        // HTML
        {
            test:    /\.html$/,
            exclude: /node_modules/,
            loader:  'html'
        },
        // LESS
        {
            test:    /\.less/,
            exclude: /node_modules/,
            loader:  'style-loader!css-loader!less-loader'
        },
        // images
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loaders: isProd ? [
                'file?hash=sha512&digest=hex&name=image.[hash].[ext]',
                'image-webpack?{progressive:true, optimizationLevel: 7, interlaced: false, pngquant:{quality: "65-90", speed: 4}}'
            ] : [
                'file?hash=sha512&digest=hex&name=image.[hash].[ext]',
                'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
            ]
        },
        // fonts
        {
            test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/font-woff&prefix=fonts&name=font.[hash].[ext]'
        }, {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/octet-stream&prefix=fonts&name=font.[hash].[ext]'
        }, {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/vnd.ms-fontobject&prefix=fonts&name=font.[hash].[ext]'
        }, {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=image/svg+xml&prefix=fonts&name=font.[hash].[ext]'
        }
    ];

    const plugins = {
        commonChunks: () => {
            return new webpack.optimize.CommonsChunkPlugin({
                name:      'vendors',
                filename:  'vendors.js',
                minChunks: (module, count) => {
                    return module.resource && module.resource.indexOf('node_modules') !== -1 && count >= 1;
                }
            });
        },
        uglifyJs: () => {
            return new webpack.optimize.UglifyJsPlugin({
                mangle:   false,
                compress: {
                    warnings: false
                }
            });
        },
        provide: () => {
            return new webpack.ProvidePlugin({
                React: 'react'
            });
        },
        define: () => {
            return new webpack.DefinePlugin({
                __DEV__:  options.env === 'dev',
                __TEST__: options.env === 'test',
                __PROD__: isProd
            });
        },
        html: (options) => {
            return new HtmlWebpackPlugin(options);
        },
        sourceMap: (path = '/') => {
            return new webpack.SourceMapDevToolPlugin({
                filename: '[file].map',
                append:   `\n//# sourceMappingURL=${path ? path : ''}[url]`
            })
        },
        ignore: (packages = []) => {
            return new webpack.IgnorePlugin(
                new RegExp(`^(${packages.join('|')})$`)
            );
        },
        hmr: () => {
            return new webpack.HotModuleReplacementPlugin();
        }
    };

    return Object.assign({
        context: context,
        resolve: resolve,
        entry:   [
            './bootstrap.js'
        ],
        output: {
            path:          !options.watch ? paths.client.tmpDest : paths.client.dest,
            publicPath:    '/',
            filename:      'bootstrap.js',
            chunkFilename: '[name].[chunkhash].js'
        },
        module: {
            loaders: loaders
        },
        plugins: [
            plugins.commonChunks(),
            plugins.define(),
            plugins.ignore(['fs', 'path']),
            plugins.provide(),
            plugins.sourceMap(),
            plugins.html({
                template: 'index.html',
                inject:   'body',
                filename: 'index.html',
                favicon:  'favicon.ico'
            }),
            plugins.hmr()
        ]
    }, {
        watch: options.watch,
        cache: true
    });

}
