import paths from '../../paths';

export default (gulp, plugins, options) => {
    return () => {
        const env       = options.env || 'dev';
        const configDir = paths.config.src;

        return gulp.src([`${configDir}/base.json`, `${configDir}/${env}.json`])
            .pipe(plugins.extend('./config.json', true, '\t'))
            .pipe(gulp.dest(paths.config.dest));

    };
};