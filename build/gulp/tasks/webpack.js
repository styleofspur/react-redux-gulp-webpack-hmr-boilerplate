import webpack from 'webpack';
import paths   from '../../paths';

import webpackConfig from '../../webpack/config';
import WebpackHelper from '../../webpack/helper';
import buildHelper   from '../../helper';

export default (gulp, plugins, options)=>{
    return async cb => {
        try{

            const webpackHelper = new WebpackHelper(plugins.util);
            const config        = webpackConfig(options);

            if (options.watch) {
                config.watch = true;
            }

            await buildHelper.removeDir(paths.client.tmpDest);

            return new Promise(async (resolve, reject) => {

                // init webpack
                const compiler = webpack(config);

                // Watch webpack
                if (config.watch) {
                    await buildHelper.removeDir(paths.client.dest);

                    compiler.watch(config, (err, stats) => {
                        if(err) return reject(err);
                        webpackHelper.handleOutput(stats);
                    });

                    return;
                }

                // Run webpack
                compiler.run((err, stats) => {
                    if(err) return reject(err);

                    webpackHelper.handleOutput(stats);

                    // Finalize gulp task..
                    gulp
                        .src(`${paths.client.tmpDest}/**/*`)
                        .pipe(gulp.dest(paths.client.dest))
                        .on('error', reject)
                        .on('end', async () => {
                            plugins.util.log(`Files in ${paths.client.tmpDest} has been moved to ${paths.client.dest}`);
                            await buildHelper.removeDir(paths.client.tmpDest);
                            resolve();
                        });
                }) ;
            });

        } catch(e) {
            cb(e);
        }
    }
}