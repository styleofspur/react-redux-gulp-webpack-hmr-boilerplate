import paths from '../../paths';

export default (gulp, plugins, options)=>{
    return () => {
        return gulp.src(`${paths.client.src}/**/!(*.js)`)
            .pipe(gulp.dest(paths.client.dest));
    }
};