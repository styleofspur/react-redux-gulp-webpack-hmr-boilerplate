import paths       from '../../paths';
import buildHelper from '../../helper';

export default ()=>{
    return cb => {
        buildHelper.removeDir(paths.base.dest)
            .then(() => cb())
            .catch(cb);
    }
};