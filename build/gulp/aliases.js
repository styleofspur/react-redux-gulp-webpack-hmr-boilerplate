import runSequence from 'run-sequence';

export default function (gulp, plugins, options) {

    runSequence.use(gulp);

    // Write aliases
    gulp
        .task('build', callback => {
            runSequence(
                'prv-cleanDist',
                'prv-createFolders',
                'prv-config',
                'prv-copy',
                'prv-babel',
                'prv-webpack',
                callback
            );
        })
        .task('watchClient', callback => {
            options.watch = true;
            runSequence('prv-webpack', callback);
        })
        .task('watchServer', callback => {
            runSequence(
                'prv-config',
                'prv-babel',
                'prv-babel-watch',
                callback
            );
        })
        .task('default', ['build']);
}
