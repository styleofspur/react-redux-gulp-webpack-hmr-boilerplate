import path from 'path';

let paths = {
    base:   {
        src:  './src',
        dest: './dist'
    },
    config: {
        get src() {
            return './build/config';
        },
        get dest() {
            return './';
        }
    },
    server: {
        get src() {
            return path.join(paths.base.src, 'server');
        },
        get dest() {
            return path.join(paths.base.dest, 'server');
        }
    },
    client: {
        get src() {
            return path.join(paths.base.src, 'client');
        },
        get dest() {
            return path.join(paths.base.dest, 'client');
        },
        get tmpDest() {
            return path.join(paths.base.dest, 'clientTemp');
        }
    },
    gulp:   {
        base:  './build/gulp',
        tasks: './build/gulp/tasks'
    }
};

export default paths;
