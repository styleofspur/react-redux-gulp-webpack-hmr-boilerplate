import fs      from 'graceful-fs';
import express from 'express';
import http    from 'http';
import https   from 'https';
import path    from 'path';

import config from '../config';

export default class Server{

    _app;
    _httpServer;
    _httpsServer;

    init(){
        this._app        = express();
        this._httpServer = http.createServer(this._app);

        if(this.useHttps){

            let baseSslFolder = path.resolve(__dirname, '..', '..', '..', 'assets', 'ssl');

            this._httpsServer = https.createServer({
                key:                fs.readFileSync(path.resolve(baseSslFolder, 'my-project.com.key')),
                cert:               fs.readFileSync(path.resolve(baseSslFolder, 'my-project.com.gd.crt')),
                ca:                 fs.readFileSync(path.resolve(baseSslFolder, 'my-project.com.gd.bundle.crt')),
                requestCert:        true,
                rejectUnauthorized: false
            }, this._app)
        }
    }

    get useHttps() {
        return config.get('https:enabled');
    }

    get app() {
        return this._app;
    }

    get httpServer() {
        return this._httpServer;
    }

    get httpsServer() {
        return this._httpsServer;
    }
}
