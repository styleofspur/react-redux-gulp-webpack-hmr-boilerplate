import bunyan from 'bunyan';
import path   from 'path';

import config               from '../config';
import { ValidationHelper } from '../helpers';
import CustomPrettyStream   from './streams/customPrettyStream';

export default class Logger{

    _logger;

    static prefix  = 'ps';
    static logsDir = path.resolve(config.get('processDir'), '..', '..', 'logs');

    constructor(name, options = {}) {

        name = `${Logger.prefix}:${name}`;

        this._logger = bunyan.createLogger({
            name:        name,
            serializers: {
                err: bunyan.stdSerializers.err
            },
            streams:     options.streams || [
                {
                    type:   'rotating-file',
                    path:   Logger.logsDir + '/general.log',
                    period: '1d',   // daily rotation
                    count:  3        // keep 3 back copies
                },
                {
                    level:  process.env.LOG_LEVEL || options.level || config.get('env:logging:level'),
                    stream: this._getStream(CustomPrettyStream),
                    type:   options.type || 'raw'
                }
            ]
        });
    }

    _getStream(Class) {
        let stream = new Class({useColor: false});
        stream.pipe(process.stdout);
        return stream;
    }

    _log(type, msg) {
        return this._logger[type](msg);
    }

    info(msg) {
        this._log('info', msg);
    }

    warn(msg) {
        this._log('info', msg);
    }

    error(err) {
        this._log('error', ValidationHelper.isError(err) ? err.message : err);
    }

    debug(msg) {
        this._log('debug', msg);
    }

    getNativeLogger() {
        return this._logger;
    }
}

