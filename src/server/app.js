import { LogHelper } from './helpers';
import Server        from './utils/server';
import config        from './config';

import middlewares from './middlewares';

export default new Promise(async (resolve, reject) => {
    try {

        const server = new Server();
        server.init();

        await middlewares(server.app);

        server.httpServer.listen(config.get('http:port'), () => {
            LogHelper.info(`HTTP Server started at port: ${server.httpServer.address().port} `);
            resolve(server.app);
        });

        if(server.useHttps){
            server.httpsServer.listen(config.get('https:port'), () => {
                LogHelper.info(`HTTPS Server started at port: ${server.httpsServer.address().port} `);
                resolve(server.app);
            });
        }
    } catch(e) {
        LogHelper.error(e);
        reject(e);
    }
});

