import VError from 'verror';

export class BaseError extends VError {
}

export default class BaseHttpError extends BaseError {
    constructor(message) {
        super(message);
    }

    toJson() {
        return {
            error:  this.message,
            status: this.status
        };
    }

    toJsonString() {
        return JSON.stringify(this.toJson());
    }
}

export class HttpInternalServerError extends BaseHttpError {
    constructor(message = 'Internal Error') {
        super(message);
        this.status = 500;
    }
}

export class HttpNotFoundError extends BaseHttpError {
    constructor(message = 'Not Found') {
        super(message);
        this.status = 404;
    }
}