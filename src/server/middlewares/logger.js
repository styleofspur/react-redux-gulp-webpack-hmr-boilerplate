import bunyanRequest from 'bunyan-request';

import { LogHelper } from '../helpers';

export default () => {
    return bunyanRequest({
        logger: LogHelper.factory('AccessLog').getNativeLogger()
    });
}