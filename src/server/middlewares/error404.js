import { HttpNotFoundError } from '../errors';

export default () => {
    return (req, res, next) => {
        next(new HttpNotFoundError());
    };
}