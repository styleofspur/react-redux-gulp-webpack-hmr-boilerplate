import expressValidator from 'express-validator';

export default () => {
    return expressValidator({
        customValidators: {},
        errorFormatter: (param, msg, value) => {
            return {
                param: param,
                message: msg,
                value: value
            };
        }
    });
}