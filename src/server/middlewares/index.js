import config    from '../config';
import xpowerBy  from './xpowerBy';
import validator from './validator';
import logger    from './logger';
import error404  from './error404';
import error     from './error';
import routers   from '../routers';

export default async app => {
    app.use(xpowerBy());
    app.use(validator());
    app.use(logger());

    routers(app);
    if (config.get('env:name') === 'dev') {
        let webpack = await require('./webpack')();
        app.use(webpack.dev);
        app.use(webpack.hot);
    }

    app.use('*', error404());
    app.use(error());
};
