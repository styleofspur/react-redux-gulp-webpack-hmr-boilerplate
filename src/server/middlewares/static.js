import path    from 'path';
import express from 'express';

export default {
    public(){
        let route = path.resolve(__dirname, '..', '..', 'client');
        return express.static(route);
    }
}