import 'babel-core/register';

import del                  from 'del';
import WriteFilePlugin      from 'write-file-webpack-plugin';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpack              from 'webpack';
import path                 from 'path';

import { LogHelper } from '../helpers';
import webpackConfig from '../../../build/webpack/config';

export default async () => {
    const options = {
        env: 'dev',
        watch: true,
        paths: {
            client: {
                get src() {
                    return path.join(__dirname, '..', '..', '..', 'src', 'client');
                },
                get dest() {
                    return path.join(__dirname, '..', '..', 'client');
                },
                get tmpDest() {
                    return path.join(__dirname, '..', '..', 'clientTemp');
                }
            },
        }
    };

    await del(options.paths.client.dest, {force: true});
    const config = webpackConfig(options);

    // add write file plugin
    config.plugins = config.plugins.concat([
        new WriteFilePlugin({ log: false })
    ]);
    // add HMR entry point
    config.entry = config.entry.concat([
        'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
    ]);

    const compiler = webpack(config);

    return {
        dev: webpackDevMiddleware(compiler, {
            publicPath:         config.output.publicPath,
            quiet:              false,
            watchOptions:       {
                aggregateTimeout: 300,
                poll:             1000
            },
            historyApiFallback: true,
            cache:              true,
            colors:             true,
            host:               '127.0.0.1',
            stats:              {
                colors:       true,
                errors:       true,
                warnings:     true,
                chunks:       false,
                version:      false,
                assets:       false,
                timings:      false,
                entrypoints:  false,
                chunkModules: false,
                chunkOrigins: false,
                cached:       false,
                cachedAssets: false,
                reasons:      false,
                usedExports:  false,
                children:     false,
                source:       false,
                modules:      false
            }
        }),
        hot: webpackHotMiddleware(compiler, {
            log:       LogHelper.log,
            path:      '/__webpack_hmr',
            heartbeat: 10 * 1000
        }),
    };

}