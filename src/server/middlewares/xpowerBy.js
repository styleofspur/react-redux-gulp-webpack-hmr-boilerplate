import config from '../config';

export default () => {
    return (req, res, next) => {
        res.header('X-Powered-By', config.get('project:name') + ' RnD');
        next();
    };
}
