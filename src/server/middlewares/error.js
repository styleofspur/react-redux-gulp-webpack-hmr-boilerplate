import { LogHelper } from '../helpers';

export default () => {
    return (err, req, res, next) => {

        if (typeof err === 'string') {
            err = new Error(err);
        }

        res.status(err.status || 500);

        LogHelper.error(err);
        req.log.info(err, 'error');

        res.json({
            error:  err.message,
            status: err.status
        });
    };
}