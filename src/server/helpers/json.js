export default class JsonHelper{
    static parse(data, cb){
        try{
            return JSON.parse(data);
        }catch(e){
            if(typeof cb === 'function') cb(e);
            throw e;
        }
    }

    static stringify(data, cb){
        try{
            return JSON.stringify(data, cb);
        }catch(e){
            if(typeof cb === 'function') cb(e);
            throw e;
        }
    }
}
