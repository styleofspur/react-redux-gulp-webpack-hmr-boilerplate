import path from 'path';

import config from '../config';

export default class EnvHelper{
    static get name(){
        return config.get('env:name');
    }

    static isDev(){
        return EnvHelper.name === 'dev';
    }

    static isProd(){
        return EnvHelper.name === 'prod';
    }

    static getProjectRoot(){
        return path.resolve(`${__filename}/../../../../`);
    }
}
