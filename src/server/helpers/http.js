export default class HttpHelper{

    static getHeader(req, headerName){
        return req && req.headers ? req.headers[headerName] : undefined;
    }

    static getAppHost(req){
        return req ? `${req.protocol}://${req.get('host')}` : undefined;
    }

}
