export default class ValidationHelper {

    static notEquals(a, b){
        if(!a || !b) return false;
        return a !== b;
    }

    static isValidDate(date){
        return !isNaN(Date.parse(date));
    }

    static isNotEmptyArray(value) {
        return ValidationHelper.isArray(value) && !!value.length;
    }

    static isString(value){
        return typeof value === 'string';
    }

    static isObject(value){
        return typeof  value === 'object';
    }

    static isError(value){
        return value instanceof Error;
    }

    static isUndefined(value){
        return typeof value === 'undefined';
    }

    static isDefined(value){
        return typeof value !== 'undefined';
    }

    static isFunction(value){
        return typeof value === 'function';
    }

    static isArray(value){
        return value instanceof Array;
    }
}