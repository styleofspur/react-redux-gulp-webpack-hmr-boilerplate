import Logger from '../utils/logger';

export default class LogHelper{

    static _logger;

    static init(){
        LogHelper._logger = new Logger('Main');
    }

    static info(msg){
        if(!LogHelper._logger) LogHelper.init();
        LogHelper._logger.info(...arguments);
    }

    static warn(msg){
        if(!LogHelper._logger) LogHelper.init();
        LogHelper._logger.info(...arguments);
    }

    static error(err){
        if(!LogHelper._logger) LogHelper.init();
        LogHelper._logger.error(err);
    }

    static debug(msg){
        if(!LogHelper._logger) LogHelper.init();
        LogHelper._logger.debug(...arguments);
    }

    static fatal(msg){
        if(!LogHelper._logger) LogHelper.init();
        LogHelper._logger.fatal(...arguments);
    }

    // Alias
    static log(msg){
        LogHelper._logger.info(...arguments);
    }

    static factory(name, options = {}){
        return new Logger(name, options);
    }
}
