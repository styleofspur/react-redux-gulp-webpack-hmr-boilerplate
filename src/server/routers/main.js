import express from 'express';
import fs      from 'graceful-fs';
import path    from 'path';

let router = express.Router();

router.get('/', (req, res, next) => {
    res.end(fs.readFileSync(path.resolve('..', 'client', 'index.html')));
});

export default router;
