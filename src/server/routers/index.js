import main from './main';

export default app => {
    app.use('/', main);
};