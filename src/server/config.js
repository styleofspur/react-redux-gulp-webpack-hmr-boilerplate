import nconf from 'nconf';
import path  from 'path';

import packageJson from '../../package.json';

nconf
    .argv()
    .env()
    .file('global', { file: path.resolve('../../config.json') });

nconf.set('version', packageJson.version);
nconf.set('processDir', process.cwd());

export default nconf;