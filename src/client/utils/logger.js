import bunyan from 'browser-bunyan';

import { EnvHelper, BrowserHelper, GeneralHelper } from '../helpers/hub';
import CustomFormattedStream                       from './streams/customFormattedStream.js';
import CustomRawStream                             from './streams/customRawStream.js';

export default class Logger{

    static nameLength = 15;
    static prefix = 'sw';

    constructor(name, options = {}){

        name = this.getAlignedName(`${Logger.prefix}:${name}`);

        this.logger = bunyan.createLogger({
            name: name,
            serializers: {
                err: bunyan.stdSerializers.err,
                obj: function(inputObj) {

                    let typeOfInput = typeof inputObj;
                    if(typeOfInput === 'object'){
                        return inputObj;
                    }

                    // Convert
                    let res = {};
                    res[typeOfInput] = inputObj;
                    return res;
                }
            },
            streams: options.streams || [
                {
                    level: options.level || (EnvHelper.isProd() ? 'info' : 'debug'),
                    stream: !BrowserHelper.isIE() && !BrowserHelper.isEdge() ? new CustomFormattedStream() : new CustomRawStream(),
                    type: options.type || 'raw'
                }
            ]
        });
    }

    getAlignedName(name){
        if(name.length < Logger.nameLength){
            return name.concat(' '.repeat(Logger.nameLength-name.length));
        }
        return name;
    }

    info(msg, obj){
        this.logInternal('info', msg, obj);
    }

    warn(msg, obj){
        this.logInternal('info', msg, obj);
    }

    error(err, obj){
        this.logInternal('error', GeneralHelper.isString(err) ? new Error(err) : err, obj);
    }

    debug(msg, obj){
        this.logInternal('debug', msg, obj);
    }

    logInternal(type, msg, obj){

        let serializers = {
            obj: obj,
            err: msg instanceof Error ? msg : undefined
        };

        return this.logger[type](...(serializers.obj || serializers.err ? [serializers, msg] : [msg]));
    }
}


