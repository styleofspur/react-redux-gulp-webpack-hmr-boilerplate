import bunyan, { ConsoleRawStream } from 'browser-bunyan';
import { EnvHelper, GeneralHelper } from '../../helpers/hub';

export default class CustomRawStream extends ConsoleRawStream {
    write(rec){
        if(!GeneralHelper.isFunction(console[rec.levelName])){
            rec.levelName = 'info';
        }

        console[rec.levelName]('[%s]: %s', rec.time.toISOString(), rec.msg);

        if(!EnvHelper.isProd()){
            if(rec.err) {
                console.dir(rec.err);
                console.log(rec.err.stack);
            }

            if(rec.obj) {
                console.dir(rec.obj);
            }
        }
    }
}