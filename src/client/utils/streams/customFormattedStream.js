import bunyan, { ConsoleFormattedStream } from 'browser-bunyan';
import { EnvHelper } from '../../helpers/hub';

export default class CustomFormattedStream extends ConsoleFormattedStream {
    write(rec){
        super.write(rec);

        if (!EnvHelper.isProd()) {
            if(rec.obj) {
                console.dir(rec.obj);
            }
        }
    }
}