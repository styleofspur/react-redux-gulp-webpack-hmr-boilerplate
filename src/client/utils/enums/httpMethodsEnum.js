/**
 * Represents a list of HTTP methods app uses
 */
export default class HttpMethodsEnum {
    static GET    = 'get';
    static POST   = 'post';
    static PUT    = 'put';
    static DELETE = 'delete';
}