/**
 * Represents a list of defined application routes.
 */
export default class AppRoutesEnum {
    static HOME = '/';
}