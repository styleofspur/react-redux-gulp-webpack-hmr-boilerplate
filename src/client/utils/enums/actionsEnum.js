/**
 * Represents a list of application actions
 */
export default class ActionsEnum {

    static COUNTER = {
        INC: 'COUNTER_INC',
        DEC: 'COUNTER_DEC'
    };
}