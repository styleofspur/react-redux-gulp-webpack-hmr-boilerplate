import AppRoutes   from './appRoutesEnum';
import HttpMethods from './httpMethodsEnum';
import Actions     from './actionsEnum';

export const AppRoutesEnum   = AppRoutes;
export const ActionsEnum     = Actions;
export const HttpMethodsEnum = HttpMethods;

if (module.hot) {
    module.hot.accept();
}