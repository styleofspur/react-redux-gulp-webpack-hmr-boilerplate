import BaseError from './base';

export default class HttpError extends BaseError{
    constructor(httpResponse){
        let message = `General Error (${httpResponse.status || 0})`;
        if(httpResponse && httpResponse.responseJSON && httpResponse.responseJSON.error){
            if(typeof httpResponse.responseJSON === 'object' && Array.isArray(httpResponse.responseJSON.reasons)){
                message = httpResponse.responseJSON.reasons[0].message;
            }else{
                message = httpResponse.responseJSON.error;
            }
        }

        super(message || 'Error message not found');
        this.status = httpResponse.status;
    }
}
