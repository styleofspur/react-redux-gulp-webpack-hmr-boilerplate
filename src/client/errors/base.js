import { GeneralHelper } from '../helpers/hub';

export default class BaseError extends Error{
    constructor(message) {
        super(message);

        this.name = this.constructor.name;
        this.message = message;

        if (GeneralHelper.isFunction(Error.captureStackTrace)) {
            Error.captureStackTrace(this, this.constructor.name);
        } else if (this.stack) {
            this.message = (this.message || '').concat('\n'+this.stack)
        }
    }

    toString() {
        return this.message || super.toString();
    }
};
