/* global require */
import selectn from 'selectn';

export default class ConfigHelper{

    static get(path) {
        return selectn(path, ConfigHelper);
    }

    static init() {
        const config = require('json!../../../config.json');
        for(let prop in config) {
            if (config.hasOwnProperty(prop)) {
                ConfigHelper[prop] = config[prop];
            }
        }
    }
};