import { compose, createStore } from 'redux';

export default class StoreHelper {

    static _store = null;

    /**
     * Initializes redux store
     */
    static init(store) {

        return new Promise((resolve, reject) => {
            StoreHelper._store = store;

            resolve();
        });
    }

    /**
     * Provides with redux store
     * @returns {*}
     */
    static getStore() {
        return StoreHelper._store;
    }

    /**
     * Provides with app starte
     * @returns {*}
     */
    static getState() {
        return StoreHelper._store.getState();
    }

    /**
     * Dispatches Redux action
     * @param action
     */
    static dispatch(action = {}) {
        this._store.dispatch(action);
    }

}
