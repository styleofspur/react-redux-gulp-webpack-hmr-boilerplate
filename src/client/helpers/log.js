import Logger    from '../utils/logger';
import EnvHelper from './env';

export default class LogHelper{

    static DEFAULT_LEVEL = EnvHelper.isProd() ? 'info' : 'debug';

    static logger = null;
    static disabled = false;

    static init(){
        return new Promise((resolve, reject)=> {
            LogHelper.logger = LogHelper.factory('App');
            resolve();
        });
    }

    /**
     *
     */
    static disable() {
        LogHelper.disabled = true;
    }

    /**
     *
     */
    static enable() {
        LogHelper.disabled = false;
    }

    /**
     *
     * @param msg
     * @param obj
     */
    static info(msg, obj) {
        if (!LogHelper.disabled) {
            LogHelper.logger.info(msg, obj);
        }
    }

    /**
     *
     * @param msg
     * @param obj
     */
    static warning(msg, obj) {
        if (!LogHelper.disabled) {
            LogHelper.logger.warn(msg, obj);
        }
    }

    /**
     *
     * @param err
     * @param obj
     */
    static error(err, obj) {
        if (!LogHelper.disabled) {
            LogHelper.logger.error(err, obj);
        }
    }

    /**
     *
     * @param msg
     * @param obj
     */
    static debug(msg, obj) {
        if (!LogHelper.disabled) {
            LogHelper.logger.debug(msg, obj);
        }
    }

    /**
     *
     * @param name
     * @param options
     * @returns {Logger}
     */
    static factory(name, options = {}) {
        options = Object.assign({level: LogHelper.DEFAULT_LEVEL}, options);
        return new Logger(name, options);
    }
}
