export default class EnvHelper {

    static isDev() {
        return typeof(__DEV__) !== 'undefined' && __DEV__;
    }

    static isTest() {
        return typeof(__TEST__) !== 'undefined' && __TEST__;
    }

    static isProd() {
        return typeof(__PROD__) !== 'undefined' && __PROD__;
    }
}