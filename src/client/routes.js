import Home from './pages/home';
import App from './containers/app';
import {AppRoutesEnum} from './utils/enums/hub';

export default {
        component: App,
        path: AppRoutesEnum.HOME,
        indexRoute: {
             component: Home
        },
        childRoutes: getRoutesPaths()
};

function getRoutesPaths() {
    let initialized = [];

    [
        // any route
        'home'
    ].forEach(pageName => {
        initialized.push(require(`./pages/${pageName}/route`));
    });

    return initialized;
}