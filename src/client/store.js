import { createStore, combineReducers, compose } from 'redux';
import * as reducers from './reducers/index';
import initDevTools from './containers/devTools/devTools';

let reducer = combineReducers(reducers);
let lastCreatedStore; // <------------------------------- remember store
const hmr = () => {
    if (__DEV__ && module.hot) {
        module.hot.accept('./reducers', () => {
            reducer = combineReducers(require('./reducers/index'));
            lastCreatedStore.replaceReducer(reducer);
        });
    }
};

hmr();

export default function() {
    let finalCreateStore = compose(
        initDevTools()
    )(createStore);
    
    const store = finalCreateStore(reducer, {});
    lastCreatedStore = store; // <------------------ remember store

    return store;
}