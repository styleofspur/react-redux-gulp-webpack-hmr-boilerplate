import { AppRoutesEnum } from '../../utils/enums/hub';

export default {
    path: AppRoutesEnum.HOME,
    getComponent(location, cb) {
        require.ensure([], (require) => {
            cb(null, require('./'))
        }, 'home');
    }
};

if (module.hot) {
    module.hot.accept();
}