import { connect } from 'react-redux';

class Home extends React.Component {

    static elementClass = 'home';

    render() {

        return (
            <div className={`${Home.elementClass}`}>
                Home Page
            </div>
        );
    }
}

export default connect()(Home);
